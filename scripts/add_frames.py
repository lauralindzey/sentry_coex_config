#!/usr/bin/env python3
"""
Adds/changes header frames in a rosbag file.

This is needed to deal with some of Sentry's early bags including MultibeamRaw
data, where for some reason the frame ID was not set for the Kongsberg.

ALSO discards the other messages in the file.
"""
import argparse
import rosbag


def edit_frame_id(frame_id, edit_topic, input_bagfile, output_bagfile):
    in_bag = rosbag.Bag(input_bagfile, 'r')
    out_bag = rosbag.Bag(output_bagfile, 'w')
    for msg_topic, msg_data, msg_tt in in_bag.read_messages():
        if msg_topic == edit_topic:
            msg_data.header.frame_id = frame_id
            out_bag.write(msg_topic, msg_data, msg_tt)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("frame_id", type=str,
                        help="ROS frame ID that will be added")
    parser.add_argument("topic", type=str,
                        help="Topic whose frame needs to be edited")
    parser.add_argument("input_bag", type=str,
                        help="Full path to input bag file")
    parser.add_argument("output_bag", type=str,
                        help="Full path to output bag file")
    args = parser.parse_args()
    edit_frame_id(args.frame_id, args.topic, args.input_bag, args.output_bag)
