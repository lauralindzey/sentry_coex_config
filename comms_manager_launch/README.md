## Overview

This launch file is meant to enable testing (almost) all components of
the coexploration stack in simulation.
This README focuses on the configuration steps to get a given subset of coexploration 
running; for more details about each piece, see their respective documentation.


#### Dependencies

Depending on options used, running the coexploration stack in simulation has dependencies on:

* [progressive_imagery](https://bitbucket.org/whoidsl/progressive_imagery): only if transferring images
* [ds_acomms_msgs](https://bitbucket.org/whoidsl/ds_msgs): some of the comms_manager control messages are defined in here
* [ds_acomms_modem](https://bitbucket.org/whoidsl/ds_acomms): micromodem driver


## Setup

#### Example data

Any example data referenced below that is too big to check into the repo itself can be 
obtained from the [Downloads](https://bitbucket.org/whoidsl/coexploration/downloads/) section.

The images and the multibeam map are from the 2018-cordes cruise using Sentry. 
The PI for this cruise did not request a data embargo (thank you!!)

The bagfiles are from the 2019-engineering cruise.

#### Lookup Table

The first step is to obtain a lookup table that the 
comms manager needs when encoding messages for acoustic transmission.
It provides a mapping from a 2-byte code to a topic and message type pair.
(Topic names are too long to encode in our bandwidth limited use cases.)

If using the example data downloaded from bitbucket, the default lookup table in the 
launch file will work. If you want to test with your own bag files, follow the steps in the 
"Setup" section of the [comms_manager README](../comms_manager/README.md).

The `topic_lookup_filename` param needs to point to the file you create.
(You can set it every time on the roslaunch command line, or edit coexploration.launch)

#### Comms_manager transport mechanism

There are three options for how the two comms managers communicate, and you must select exactly one.
They all default to false to make command-line control easier.

`roslaunch coexploration.launch use_direct_connection:=true`

`roslaunch coexploration.launch use_mock_modems:=true`

`roslaunch coexploration.launch use_micromodem_testbox:=true`

#### Streaming Background Data

The coexploration stack currently includes two libraries that make use of the 
comms_manager's ability to send background data.
By default, both are disabled.

** Progressive Imagery **

Gobysoft implemented the progressive_imagery library, and we have demonstrated its use to send images from subsea to topside.

set `run_progressive_imagery:=true` to launch nodes associated with progressive imagery.

Once it is running, it will attempt to copy all images in the directory specified by the
`pi_send_directory` argument. 

** Quadtree **

Set `run_quadtree:=true` to launch nodes associated with the quadtree.

Once it is running, it will start streaming updates from the map that is is creating.
This map can either be pre-loaded from a grid file, or created live from sensor messages.


## Examples

TODOs:

* Micromodem testbox?
* Progressive Imagery with request-by-time?

**Using Comms Manager to relay arbitrary ROS messages**

(This functionality has been tested on Sentry during a dive, and exists in the sentry_config launch files)

First, setup your data. Either:

1. Use example data from sentry:
    * download the [`sensors_2019-10-13-06-30-25_6.bag`](https://bitbucket.org/whoidsl/coexploration/downloads/sensors_2019-10-13-06-30-25_6.bag) file for an assortment of Sentry's sensor data
    * use [`20200305_coexploration_topics.txt`](20200305_coexploration_topics.txt) for the lookup table. (It will work for this bag file, and all others provided in these examples)
2. Use your own bag file; this requires generating your own lookup table as described above

Next, launch the comms manager stack:

* Update parameters:
    * param `topic_lookup_filename` needs to match the data you chose
* `cd ~/sentry_ws/src/coexploration/launch`
* `roslaunch coexploration.launch use_direct_connection:=true use_replay_bagfile:=true`
* `rosbag play --clock sensors_2019-10-13-06-30-25_6.bag`
* In the TDMA GUI, select "12/minute", then click "Update" and "Publish"  (or your bagfile)
* Figure out what topics you want to relay from "subsea" to "topside" (`rostopic list`)
* In the Comms Manager Configuration GUI, on the Subsea tab, select topics and click "Add". NB: This list includes all of the topics that are available in the lookup table, not only the ones in your bagfile.
* After adding all the topics you are interested in, click "Publish"
* Watch the streaming data!
    * Look at `rqt_graph` to see the connections between the comms managers and how data is relayed.
    * Use `rqt_plot` to see a time series. If using Sentry's bagfile, there are several sources of depth data (paro and CTD), or you could look at how OBS and ORP vary ... have fun!
    * `rosrun comms_manager comms_echoer.py rx_packet:=/acomms/data_up` for a terminal display of all messages that are transmitted in each packet.

See the [comms_manager README](../comms_manager/README.md) for more detailed information about how it relays messages and how to control it using the TDMA and Queue GUIs.

**Coexploration + image transfer, using pre-staged images:**

(This functionality has been tested on Sentry during a dive, and exists in the sentry_config launch files)

* Obtain images:
    * [Download from bitbucket](https://bitbucket.org/whoidsl/coexploration/downloads/)
    * Use your own. This has been tested with .tif, though some other files types may be supported. The filenames are expected to be `image_####.tif`, where valid numbers are between 0 and 1,000,000.
* Set arg `pi_send_directory` to point to the directory with images. 
* Empty `pi_receive_directory`
* `cd ~/sentry_ws/src/coexploration/launch`
* `roslaunch coexploration.launch use_direct_connection:=true run_progressive_imagery:=true`
* In the TDMA GUI, select "12/minute", then click "Update" and "Publish"
* In the Comms Manager Configuration GUI, select the "Progressive Imagery" radiobutton at the top, then click "Publish"
* In the Progressive Imagery GUI, watch the images resolve.

See the [image_utils README](../image_utils/README.md) for more detailed instructions for interacting with Progressive Imagery.

**Coexploration + quadtree transfer with a pre-staged map:**

(This functionality has been tested on Sentry on deck, and exists in the sentry_config launch files)

* [Download an example grid file from bitbucket](https://bitbucket.org/whoidsl/coexploration/downloads/sentry504_20181025_1532_rnv_tide_equal_1.00x1.00_BV01.grd). This data is from the 2018-cordes cruise, for which the PI did not request an embargo. (Thank you!!)
* `cd ~/sentry_ws/src/coexploration/launch`
* `roslaunch coexploration.launch use_direct_connection:=true run_quadtree:=true quadtree_use_prestaged_map:=true quadtree_input_grid_filename:=/path/to/your/grid/file`
* In the TDMA GUI, select "12/minute", then click "Update" and "Publish"
* In the Comms Manager Configuration GUI, select the "Quadtree" radiobutton at the top, then click "Publish"
* In the Quadtree GUI, watch the streaming updates fill out the map. You may need to adjust the spatial extent and/or the color limits.

See the [quadtree README](../quadtree/README.md) for more detailed instructions for interacting with Quadtree grid transfer.

**Coexploration + quadtree with multibeam data published as pointclouds:**

This requires either the POS dataset (unfortunately not publicly available) to run with default settings, or your own bagfile that has pointclouds, which will require some additional parameter setting.

* First, set parameters (for POS data, these should be default or commented out in the bag file):
    * arg `quadtree_map_frame` needs to be set to the frame that the map should be created in
    * arg `multibeam_pointcloud_topic` needs to be set to input topic for the `sensor_msgs/PointCloud2` messages
    * param `robot_description` needs to point to a urdf that can be used by the robot_state_publisher to generate a tf tree. (Needs to provide a transform from the pointcloud's frame to the map frame.
* `cd ~/sentry_ws/src/coexploration/launch`
* `roslaunch coexploration.launch use_direct_connection:=true run_quadtree:=true use_replay_bagfile:=true`
* `cd /path/to/pos/bagfiles`; `rosbag play njr_2020-01-04-21-17-17_0.bag --start=1800 --rate=12 --clock`
* Follow the same steps as above for TDMA/Comms Manager/Quadtree GUIs

**Coexploration + quadtree with multibeam data published as MultibeamRaw**

Sentry's 2019-engineering cruise generated test data that we can use for this.

* Download the following files from bitbucket (or provide your own):
    * [kongsberg_2019-10-13-06-30-25_6.bag](https://bitbucket.org/whoidsl/coexploration/downloads/kongsberg_2019-10-13-06-30-25_6.bag) -- this contains the MultibeamRaw messages
    * [tf_2019-10-13-06-30-26_6.bag](https://bitbucket.org/whoidsl/coexploration/downloads/tf_2019-10-13-06-30-26_6.bag) -- this contains the transformation from base_link to odom_dr, our map frame
* Clone https://www.bitbucket.org/whoidsl/sentry_config to your workspace
* Then, set parameters. For Sentry, depending on the parameter, they'll either already be the default or the correct value will be present but commented out.
    * arg `quadtree_map_frame` needs to be set to the frame that the map should be created in
    * arg `multibeam_raw_topic` needs to be set to input topic for the `ds_multibeam_msgs/MultibeamRaw` messages
    * param `robot_description` needs to point to sentry_config/vehicle_model/sentry_runtime.urdf (or a corresponding one for your robot and bagfiles) 
* `cd ~/sentry_ws/src/coexploration/launch`
* `roslaunch coexploration.launch use_direct_connection:=true run_quadtree:=true use_replay_bagfile:=true`
* `cd /path/to/sentry/bagfiles`; `rosbag play --rate=12 --clock kongsberg_2019-10-13-06-30-25_6.bag tf_2019-10-13-06-30-26_6.bag` 
* Select a region that is patchy in the streamed data and request it in higher resolution.
* Follow the same steps as above for TDMA/Comms Manager/Quadtree GUIs


