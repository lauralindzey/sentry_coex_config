<!--
    Example launch file for running the coexploration stack in simulation.

    params/args will be set here if they are used by multiple modules,
    or if they are likely to be changed by the user.

    All other configuration arguments are pushed down into the individual
    xml files for comms_manager/progressive_imagery/quadtree that actually
    launch the nodes.
-->

<launch>


  <param name="/topic_lookup_filename" value="$(find comms_manager)/../launch/20200305_coexploration_topics.txt"/>


  <!-- Whether to run with replayed data or in a live system -->
  <arg name="use_replay_bagfile" default="false"/>

  <!-- Whether to launch the topside nodes -->
  <arg name="run_topside" default="true"/>

  <!-- Whether to launch the subsea nodes -->
  <arg name="run_subsea" default="true"/>

  <!-- How the topside / subsea comms_managers communicate. Valid options:
       * direct: directly subscribe to each other's published topics
       * mock: Use mock micromodems (they provide the MicromodemData ROS interface, and communicate with each other using UDP)
       * testbox: Use the micromodem testbox.
       They all default to false to force the user to select one on the
       command line. (roslaunch doesn't seem to have a clean way to force
       exactly one of these to be true.)
  -->
  <arg name="use_direct_connection" default="false"/>
  <arg name="use_mock_modems" default="false"/>
  <arg name="use_micromodem_testbox" default="false"/>

  <!-- namespace prepended to republished messages to avoid collisions with the original -->
  <arg name="topside_publication_prefix" value="/republished"/>
  <!-- namespace for messages published topside that should be forwarded to subsea -->
  <arg name="topside_subscription_prefix" value="/transmit"/>


  <!-- Which background data utilities should be started -->
  <arg name="run_progressive_imagery" default="false"/>
  <!-- The directory for to-be-transmitted images -->
  <arg name="pi_send_directory" default="$(find progressive_imagery)/test/tx"/>
  <arg name="pi_receive_directory" default="$(find progressive_imagery)/test/rx"/>


  <arg name="run_quadtree" default="false"/>
  <!-- Where the quadtree receiver will put the received grids -->
  <arg name="quadtree_rx_grid_directory" value="$(find quadtree)/rx_grids"/>
  <!-- When running with streaming sensor data, what frame to transform it into before creating the map -->
  <!-- Use this for POS data
  <arg name="quadtree_map_frame" value="/odom_filt"/>
  -->
  <!-- use this for Sentry data -->
  <arg name="quadtree_map_frame" value="/odom_dr"/>

  <!-- Used for sending pre-staged maps (as a way to test quadtree without sensor bagfiles -->
  <!-- <arg name="quadtree_input_grid_filename" value="/home/llindzey/Documents/NOAAcoexploration/multibeam/sentry546/sentry536_20190930_0357_rnv_tide_1.00x1.00_BV02.grd"/> -->
  <arg name="quadtree_input_grid_filename" default="/home/llindzey/SentryData/2018-cordes/products/sentry504/multibeam/grids/sentry504_20181025_1532_rnv_tide_1.00x1.00_BV01.grd"/>
  <arg name="quadtree_use_prestaged_map" default="false"/>

  <!-- The quadtree is capable of handling either PointCloud or MultibeamRaw messages.
       There isn't a switch to enable/disable on a per-topic basis; instead, this assumes
       that when replaying bagfiles only one of the two data types / topics will be valid. -->
  <!-- Topic for building a map based on sensor_msgs/PointCloud2 messages -->
  <!-- (This default works for POS data collected by Ian) -->
  <arg name="multibeam_pointcloud_topic" value="/njr/sensors/norbit/pointcloud"/>
  <!-- Topic for building a map based on ds_multibeam_msgs/Multibeamraw messages -->
  <!-- (This default works for Sentry data from 2019-engineering) -->
  <arg name="multibeam_raw_topic" value="/sentry/sonars/kongsberg/mbraw"/>

  <!-- If replaying from a bagfile, all nodes need to use bag-synchronized rostime. -->
  <group if="$(arg use_replay_bagfile)">
    <param name="/use_sim_time" value="true"/>
    <!-- Hacky way to get around static transforms only being published at the start of the bagfile -->
    <!-- This value works for the POS dataset -->
    <!-- <param name="robot_description" textfile="$(find pos_config)/urdf/pos_runtime.urdf"/> -->
    <param name="robot_description" textfile="$(find sentry_config)/vehicle_model/sentry_runtime.urdf"/>
    <node name="robot_state_publisher" pkg="robot_state_publisher" type="robot_state_publisher"/>
  </group>
  <!-- -->

  <!-- Topics used for controlling the comms_managers -->
  <arg name="topside_acks_topic" default="/topside/acomms/manager/acks_expected"/>
  <arg name="topside_queue_topic" default="/topside/acomms/manager/queue_definition"/>
  <arg name="topside_tdma_topic" default="/topside/acomms/manager/tdma_slots"/>

  <arg name="subsea_acks_topic" default="/sentry/acomms/manager/acks_expected"/>
  <arg name="subsea_queue_topic" default="/sentry/acomms/manager/queue_definition"/>
  <arg name="subsea_tdma_topic" default="/sentry/acomms/manager/tdma_slots"/>

  <!-- Services and topics for the background data -->

  <!-- Quadtree-specific topics -->
  <arg name="quadtree_metadata_topic" value="/sentry/coexploration/quadtree/metadata"/>
  <arg name="quadtree_sender_stats_topic" value="/sentry/coexploration/quadtree/stats"/>

  <arg name="quadtree_requested_grids_topic" value="/topside/coexploration/quadtree/requested_regions"/>
  <arg name="saved_grids_topic" value="/topside/coexploration/quadtree/saved_grids"/>
  <arg name="quadtree_subsea_stats_topic" value="/republished/sentry/coexploration/quadtree/stats"/>
  <!-- NB: These names must match those defined in generate_topic_lookup.py's config file; there is no mechanism for them to be captured from a previous dive's bagfile due to services not being logged. -->
  <arg name="quadtree_subsea_service" value="/sentry/coexploration/quadtree/sender/data_request"/>
  <arg name="quadtree_topside_topic" value="/republished/topside/coexploration/quadtree/data"/>
  <arg name="quadtree_topside_service" value="/topside/coexploration/quadtree/receiver/data_request"/>
  <arg name="quadtree_subsea_topic" value="/sentry/coexploration/quadtree/data"/>


  <!-- Progressive Imagery topics -->
  <!-- service for the sim QM to request next data chunk from progressive imagery pipeline -->
  <arg name="pi_sender_service" value="/sentry/coexploration/progressive_imagery/sender/data_request"/>
  <arg name="pi_receiver_service" value="/topside/coexploration/progressive_imagery/receiver/data_request"/>
  <!-- Topic for sim modem/QM to send data to progressive imagery pipeline -->
  <arg name="pi_sender_topic" value="/sentry/coexploration/progressive_imagery/data"/>
  <arg name="pi_receiver_topic" value="/topside/coexploration/progressive_imagery/data"/>
  <!-- Topic for status updates from the sender. -->
  <arg name="pi_sender_progress_topic" value="/sentry/coexploration/progressive_imagery/transfer_progress"/>
  <!-- Topic for setting priorities -->
  <arg name="pi_priorities_topic" value="/sentry/coexploration/progressive_imagery/queue_update"/>

  <!-- Default comms manager configuration; this is used to initialize the GUI -->
  <!-- kinetic doesn't seem to support type="yaml", and I haven't
       found a way to load an array whose values depend on arguments
       specified in the launch file. (Directly loading from a .yaml
       can get an array on the param server, but doesn't capture args)
       So, this workaround assumes that yaml.load() will be called by
       the node itself. This is UGLY. -->
  <!-- NB: do NOT prepend "transmit" here; that is controlled by another
       parameter in the comms_manager -->
  <!-- NB: At a minimum, topside needs to transmit tdma and queue updates! -->
       <arg name="default_topside_topics" value='["$(arg subsea_queue_topic)", "$(arg subsea_tdma_topic)"]'/>
       <arg name="default_subsea_topics" value='[]'/>

  <!-- The GUI groups available background data modes to avoid the user having
       to select the corresponding DATA_SRVs from two different dropdowns.
       It also allows (optional) specification of associated topics that should
       be included in the queue definition. (e.g. statups updates)
       This parameter is a dict of tuples:
       {DISPLAY_NAME: (SUBSEA_SRV, TOPSIDE_SRV, [SUBSEA_MSGs], [TOPSIDE_MSGs])}
       Again, I wish I could load this directly with yaml...
  -->
  <arg name="background_data_modes" value='{"Progressive Imagery": ["$(arg pi_sender_service)", "$(arg pi_receiver_service)", ["$(arg pi_sender_progress_topic)"], ["$(arg pi_priorities_topic)"]], "Quadtree": ["$(arg quadtree_subsea_service)", "$(arg quadtree_topside_service)", ["$(arg quadtree_metadata_topic)"], []]}'/>


  <!-- Start the comms_manager and any modem drivers -->
  <include file="$(find comms_manager)/../launch/comms_manager.xml">
    <arg name="run_topside" value="$(arg run_topside)"/>
    <arg name="run_subsea" value="$(arg run_subsea)"/>
    <arg name="use_direct_connection" value="$(arg use_direct_connection)"/>
    <arg name="use_mock_modems" value="$(arg use_mock_modems)"/>
    <arg name="use_micromodem_testbox" value="$(arg use_micromodem_testbox)"/>

    <arg name="topside_tdma_topic" value="$(arg topside_tdma_topic)"/>
    <arg name="subsea_tdma_topic" value="$(arg subsea_tdma_topic)"/>
    <arg name="topside_queue_topic" value="$(arg topside_queue_topic)"/>
    <arg name="subsea_queue_topic" value="$(arg subsea_queue_topic)"/>
    <arg name="topside_acks_topic" value="$(arg topside_acks_topic)"/>
    <arg name="subsea_acks_topic" value="$(arg subsea_acks_topic)"/>

    <arg name="topside_publication_prefix" value="$(arg topside_publication_prefix)"/>
    <arg name="topside_subscription_prefix" value="$(arg topside_subscription_prefix)"/>

    <arg name="default_topside_topics" value="$(arg default_topside_topics)"/>
    <arg name="default_subsea_topics" value="$(arg default_subsea_topics)"/>
    <arg name="background_data_modes" value="$(arg background_data_modes)"/>
  </include>

  <!-- Start progressive_imagery -->
  <include file="$(find comms_manager)/../launch/progressive_imagery.xml" if="$(arg run_progressive_imagery)">
    <arg name="run_topside" value="$(arg run_topside)"/>
    <arg name="run_subsea" value="$(arg run_subsea)"/>
    <arg name="topside_publication_prefix" value="$(arg topside_publication_prefix)"/>
    <arg name="topside_subscription_prefix" value="$(arg topside_subscription_prefix)"/>
    <arg name="subsea_acks_topic" value="$(arg subsea_acks_topic)"/>
    <arg name="topside_acks_topic" value="$(arg topside_acks_topic)"/>

    <arg name="pi_send_directory" value="$(arg pi_send_directory)"/>
    <arg name="pi_receive_directory" value="$(arg pi_receive_directory)"/>
    <arg name="pi_sender_service" value="$(arg pi_sender_service)"/>
    <arg name="pi_receiver_service" value="$(arg pi_receiver_service)"/>
    <arg name="pi_sender_topic" value="$(arg pi_sender_topic)"/>
    <arg name="pi_receiver_topic" value="$(arg pi_receiver_topic)"/>

    <arg name="pi_sender_progress_topic" value="$(arg pi_sender_progress_topic)"/>
    <arg name="pi_priorities_topic" value="$(arg pi_priorities_topic)"/>
  </include>

  <!-- Start quadtree -->
  <include file="$(find comms_manager)/../launch/quadtree.xml" if="$(arg run_quadtree)">
    <arg name="run_topside" value="$(arg run_topside)"/>
    <arg name="run_subsea" value="$(arg run_subsea)"/>
    <arg name="topside_publication_prefix" value="$(arg topside_publication_prefix)"/>
    <arg name="topside_subscription_prefix" value="$(arg topside_subscription_prefix)"/>
    <arg name="subsea_acks_topic" value="$(arg subsea_acks_topic)"/>
    <arg name="topside_acks_topic" value="$(arg topside_acks_topic)"/>

    <arg name="quadtree_metadata_topic" value="$(arg quadtree_metadata_topic)"/>
    <arg name="quadtree_sender_stats_topic" value="$(arg quadtree_sender_stats_topic)"/>

    <arg name="quadtree_subsea_topic" value="$(arg quadtree_subsea_topic)"/>
    <arg name="quadtree_subsea_service" value="$(arg quadtree_subsea_service)"/>
    <arg name="quadtree_topside_topic" value="$(arg quadtree_topside_topic)"/>
    <arg name="quadtree_topside_service" value="$(arg quadtree_topside_service)"/>

    <arg name="quadtree_rx_grid_directory" value="$(arg quadtree_rx_grid_directory)"/>
    <arg name="quadtree_input_grid_filename" value="$(arg quadtree_input_grid_filename)"/>

    <arg name="multibeam_pointcloud_topic" value="$(arg multibeam_pointcloud_topic)"/>
    <arg name="multibeam_raw_topic" value="$(arg multibeam_raw_topic)"/>

    <arg name="quadtree_map_frame" value="$(arg quadtree_map_frame)"/>
    <arg name="quadtree_use_prestaged_map" value="$(arg quadtree_use_prestaged_map)"/>


  </include>


</launch>
