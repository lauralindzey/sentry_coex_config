## Quick-Start

* Download example data from bitbucket
* Progressive Imagery setup:
  * Put images you want to transfer into ~/catkin_ws/src/sentry_coex_config/pi_data/tx
  * create directory for receiving images (clear it if it already exists): ~/catkin_ws/src/sentry_coex_config/rx

* term0$ `roscore`
* term1$ `roslaunch sentry_coex_config sentry_coex.launch sim_comms:=true use_replay_bagfile:=true run_scalar_transfer:=true run_quadtree:=true run_progressive_imagery:=true`
* term2$ `roslaunch sentry_coex.launch sim_comms:=true use_replay_bagfile:=true run_topside:=true run_scalar_transfer:=true run_quadtree:=true run_progressive_imagery:=true`
* term3 will be for bagfiles.
  * testing scalar data: $ `rosbag play --clock -r 10 sensors_2019-10-13-06-30-25_6.bag --topics /sentry/sensors/orp/a2d2 /sentry/sensors/paro/depth /sentry/sensors/obs/a2d2 /sentry/sensors/sbe49_ctd/ctd`
  * testing quadtree transfer: $ `rosbag play --rate=10 --clock kongsberg_2019-10-13-06-30-25_6.bag tf_2019-10-13-06-30-26_6.bag`

Four GUIs will pop up.
* "Message Queue Node Configuration" shows every queue that the topside and subsea message_queue_nodes are configured to listen to and forward along. Use the checkboxes and text entry fields to enable/disable individual queues or adjust priorities.
* "Scalar Grid GUI" Is currently in flux; it will show dORP hits and requested ORP data. To request data, click the "Time Selection Active" checkbox, then click-and-drag in the dORP/dt plot to define the requested time span. Edit the "Requested Rate" field, then click "Send"
* "Progressive Imagery Status" This works identically to the version in the comms_manager configuration; see that documentation.
* "Quadtree Grid GUI" Also identical to the comms_manager version.




## Prepping multibeam data

The Kongsberg data that I have access to does NOT have a frame ID included in its header.
So, for any file that we want to use, use the `add_frames.py` script to create a new bagfile:

./add_frames.py reson_link /sentry/sonars/kongsberg/mbraw sonars_2019-12-20-01-53-18_2.bag sonars_2019-12-20-01-53-18_2.bag.with_frame


Additionally, we need `/tf` messages from the globals_* bags; can reduce the number of published topics by creating new tf-only bags:
~~~bash
for gg in globals_*.bag; do echo $gg; tf=`echo $gg | sed s/globals/tf/`; echo $tf; rosbag filter $gg $tf "topic=='/tf'"; done
~~~


## Configuring ros_acomms

### Scalar data transfer

For each scalar topic, make sure that the max_length parameter for the scalar_senter
is less than or equal to that defined in the codec. Otherwise, the message will be
truncated and incorrect timestamps will be applied. (To save bandwidth, only start/end
times are published, not a full vector of them.)

Right now, the encoding is done on a per-packet basis, so there is no need to make
this small enough to fit into a single 256-byte frame.

Additionally, the codec's queue_maxsize parameter will cause messages to be dropped if
too many updates are requested at once.


### Quadtree



### Progressive Imagery
