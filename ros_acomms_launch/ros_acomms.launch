<?xml version="1.0"?>
<launch>

  <!-- Whether to launch the topside nodes -->
  <arg name="run_topside" default="false"/>

  <!-- Whether to run ros_acomms -->
  <arg name="sim_comms" default="false"/>

  <env name="ROSCONSOLE_FORMAT" value="[${severity}] [${time}] [${node}]: ${message}"/>

  <!-- Start ros_acomms and any related modem drivers -->
  <group if="$(arg run_topside)">

    <node name="queue_config_gui" pkg="ros_acomms_shims" type="queue_configuration_gui.py">
      <rosparam param="topside_codecs" command="load" file="$(find sentry_coex_config)/ros_acomms_config/sentry_topside_codec_config.yaml"/>
      <rosparam param="subsea_codecs" command="load" file="$(find sentry_coex_config)/ros_acomms_config/sentry_subsea_codec_config.yaml"/>
      <rosparam param="message_queue_nodes" subst_value="True">
        - name: subsea
          codec_filename:  "$(find sentry_coex_config)/ros_acomms_config/sentry_subsea_codec_config.yaml"
          status_topic: /republished/modem0/queue_status
          priority_command_topic: /topside/modem0/queue_priority
          enable_command_topic: /topside/modem0/queue_enable
        - name: topside
          codec_filename: "$(find sentry_coex_config)/ros_acomms_config/sentry_topside_codec_config.yaml"
          status_topic: /modem1/queue_status
          priority_command_topic: /modem1/queue_priority
          enable_command_topic: /modem1/queue_enable
      </rosparam>
      <remap from="topside_status" to="/modem1/queue_status"/>
      <remap from="subsea_status" to="/republished/modem0/queue_status"/>
    </node>

    <node name="topside_queue_monitor" pkg="ros_acomms_shims" type="queue_monitor.py" output="screen">
      <rosparam param="codec_config" command="load" file="$(find sentry_coex_config)/ros_acomms_config/sentry_topside_codec_config.yaml"/>
      <rosparam param="background_data" command="load" file="$(find sentry_coex_config)/ros_acomms_config/topside_queue_monitor.yaml"/>
      <remap from="queue_status" to="/modem1/queue_status"/>
    </node>


    <group ns="modem1">

      <node name="packet_dispatch" pkg="ros_acomms" type="packet_dispatch_node.py">
        <rosparam param="packet_codecs" command="load" file="$(find sentry_coex_config)/ros_acomms_config/sentry_subsea_codec_config.yaml" />
      </node>

      <!-- Configurable at runtime using:
          * priority_update service (changing priorities)
          * queue_active service (enabling/disabling queues)
          Data is retrieved using get_next_packet_data
          QUESTION: Can these services be called over an acoustic link?
          QUESTION: Can you ever pack multiple messages from a queue in the same packet?
            -->
      <node name="message_queue_node" pkg="ros_acomms" type="message_queue_node.py">
        <rosparam param="packet_codecs" command="load" file="$(find sentry_coex_config)/ros_acomms_config/sentry_topside_codec_config.yaml" />
      </node>

      <node name="tdma" pkg="ros_acomms" type="tdma_node.py">
        <param name="active_slots" value="1"/>
        <rosparam command="load" file="$(find sentry_coex_config)/ros_acomms_launch/tdma_node.yaml"/>
      </node>

      <group if="$(arg sim_comms)">
        <node name="modem_sim_node" pkg="ros_acomms" type="modem_sim_node.py">
          <param name="center_frequency_hz" value="10000" type="int" />
          <param name="bandwidth_hz" value="5000" type="int" />
          <param name="SRC" value="1" type="int" />
          <param name="modem_location_source" value="local_service" />
        </node>
      </group> <!-- topside sim_comms -->


      <group unless="$(arg sim_comms)">
        <node name="acomms_driver_node" pkg="ros_acomms" type="acomms_driver_node.py">
          <param name="modem_connection_type" value="serial"/>
          <param name="modem_serial_port" value="/dev/ttyUSB1" type="str" />
          <!--<param name="modem_remote_host" value="192.168.100.145"/>
          <param name="modem_local_host" value=""/>
          <param name="modem_remote_port" value="13004" type="int"/>
          <param name="modem_local_port" value="13025" type="int"/-->
          <param name="SRC" value="1" type="int" />
          <param name="modem_location_source" value="local_service" />
          <rosparam param="modem_config">
              BND: 0
              FC0: 10000
              BW0: 2000
              psk.packet.mod_hdr_version: 1
              PCM: 0
              SRC: 1
              pwramp.txlevel: 0
          </rosparam>
        </node>
      </group>  <!-- topside vehicle_comms -->
    </group> <!-- modem 1 namespace -->
  </group>  <!-- run_topside-->

  <!-- Subsea is mostly the same as topside, with a few exceptions:
      * packet_dispatch and message_queue_node use complimentary configs
      * simulation includes nodes that simulate performance / packet loss / etc.
      -->
  <group unless="$(arg run_topside)">

    <node name="subsea_queue_monitor" pkg="ros_acomms_shims" type="queue_monitor.py" output="screen" >
      <rosparam param="codec_config" command="load" file="$(find sentry_coex_config)/ros_acomms_config/sentry_subsea_codec_config.yaml"/>
      <rosparam param="background_data" command="load" file="$(find sentry_coex_config)/ros_acomms_config/subsea_queue_monitor.yaml"/>
      <remap from="queue_status" to="/modem0/queue_status"/>
    </node>

    <group ns="modem0">

      <node name="packet_dispatch" pkg="ros_acomms" type="packet_dispatch_node.py">
        <rosparam param="packet_codecs" command="load" file="$(find sentry_coex_config)/ros_acomms_config/sentry_topside_codec_config.yaml" />
      </node>

      <node name="message_queue_node" pkg="ros_acomms" type="message_queue_node.py">
        <rosparam param="packet_codecs" command="load" file="$(find sentry_coex_config)/ros_acomms_config/sentry_subsea_codec_config.yaml" />
      </node>

      <node name="tdma" pkg="ros_acomms" type="tdma_node.py">
        <param name="active_slots" value="0"/>
        <rosparam command="load" file="$(find sentry_coex_config)/ros_acomms_launch/tdma_node.yaml"/>
      </node>

      <group if="$(arg sim_comms)">
          <node name="modem_sim_node" pkg="ros_acomms" type="modem_sim_node.py">
              <param name="center_frequency_hz" value="10000" type="int" />
              <param name="bandwidth_hz" value="5000" type="int" />
              <param name="SRC" value="0" type="int" />
              <param name="modem_location_source" value="local_service" />
          </node>

      </group> <!-- subsea sim_comms -->


      <group unless="$(arg sim_comms)">
        <node name="acomms_driver_node" pkg="ros_acomms" type="acomms_driver_node.py">
          <param name="modem_connection_type" value="serial"/>
          <param name="modem_serial_port" value="/dev/ttyUSB0" type="str" />
          <!--param name="modem_remote_host" value="192.168.100.100"/>
          <param name="modem_local_host" value=""/>
          <param name="modem_remote_port" value="6767" type="int"/>
          <param name="modem_local_port" value="6768" type="int"/-->
          <param name="use_legacy_packets" value="False" type="bool" />
          <param name="SRC" value="0" type="int" />  <!-- TODO: This seems to duplicate the below SRC? -->
          <param name="modem_location_source" value="local_service" />
          <rosparam param="modem_config">
              BND: 0
              FC0: 10000
              BW0: 2000
              psk.packet.mod_hdr_version: 1
              PCM: 0
              SRC: 0
              pwramp.txlevel: 0
              xmit.txinhibit: 0
          </rosparam>
        </node> <!-- acomms_drier_node-->
      </group> <!-- subsea vehicle_comms -->
    </group> <!-- modem 0 namespace -->
  </group>  <!-- run_subsea-->


  <!-- Run these nodes only on the topside computer + if in sim, but w/o a namespace-->
  <group if="$(arg sim_comms)">
    <group if="$(arg run_topside)">
      <node name="sim_packet_performance_node" pkg="ros_acomms" type="sim_packet_performance_node.py" />

      <node name="platform_location_node" pkg="ros_acomms" type="platform_location_node.py"  />

      <node name="sim_transmission_loss_node" pkg="ros_acomms" type="sim_transmission_loss_node.py"  >
        <param name="water_depth" value="20" type="int" />
        <param name="bellhop_arrivals" value="false" type="bool" />
      </node>
    </group>
  </group>

</launch>
