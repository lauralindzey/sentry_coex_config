## sentry_coex_config

Launch and config files for running the coexploration stack
using sentry's data topics.

The main coexploration nodes have been tested with two different
acoustic communications stacks: comms_manager, which was developed
as part of the original coex effort; and ros_acomms, which is being
developed by the WHOI acomms group.

This package includes launch files for configuring both acoustic
communications stacks -- see comms_manager_launch, and
ros_acomms_launch/ros_acomms_config.
